document.querySelector(".block__element")
    .addEventListener("click", function (e) {
    if (e.target.tagName === "IMG") {
      document.querySelector(".main-image").src = e.target.src;
      document
        .querySelectorAll(".block__element img")
        .forEach(function (img) {
          img.classList.remove("is-selected");
        });
      e.target.classList.add("is-selected");
    }
  });